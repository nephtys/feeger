package Mocks

import Interfaces.FileSystemAccess

import scala.collection.mutable

/**
  * Created by Christopher on 28.04.2016.
  */
class MockFileSystemAccess extends FileSystemAccess {
  private val map : scala.collection.mutable.Map[String, String] = mutable.Map.empty[String, String]

  override def getSingleFile(filepath: String): Option[String] = map.get(filepath)

  override def writeSingleFile(filepath: String, text: String): Boolean = {
    map.put(filepath, text)
    map.get(filepath).contains(text)
  }
}
