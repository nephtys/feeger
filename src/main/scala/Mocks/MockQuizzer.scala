package Mocks

import Interfaces.Quizzable

/**
  * Created by Christopher on 28.04.2016.
  */
class MockQuizzer extends Quizzable {
  override def ask[T](question: String): T = ???
}
