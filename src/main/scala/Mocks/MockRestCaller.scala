package Mocks

import Implementations.Configuration
import Interfaces.{MinimalUser, RestCallable}

/**
  * Created by Christopher on 28.04.2016.
  */
class MockRestCaller extends RestCallable{
  override def call()(implicit config: Configuration): Seq[MinimalUser] = ???
}
