package Implementations

/**
  * Created by Christopher on 28.04.2016.
  */
case class Configuration(
                          sewobeUsername : String = "RSERVICE",
                          sewobePassword : String = """""",
                          sewobeQuery : Int = 80,
                          sewobeUrl : String = """""",
                          chargeTitle : String = """some reason""",
                          chargeDesc : String = """some reason to charge money""",
                          chargeCents : Int = 1350,
                          onlyChargeCats : Seq[String] = Seq("""vorl. Ka""", """aktiv Ka""", """inaktiv Ka"""),
                          decimalDelimiter : String = ",",
                          csvDelimiter : String = ";"

                        ) extends SewobeConfigurable {

}

