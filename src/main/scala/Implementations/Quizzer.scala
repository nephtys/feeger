package Implementations

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import Interfaces.Quizzable

import scala.util.Try

/**
  * Created by Christopher on 28.04.2016.
  */
class Quizzer extends Quizzable {
  override def ask[T](question: String): T = ???

  def askLocalDate(question : String) : LocalDate = {
    println(question)
    println("Example for input is: 2016-02-29")
    var res : Option[LocalDate] = None
    while(res.isEmpty) {
      val raw = scala.io.StdIn.readLine(s"Hit ENTER to finish your input ...${System.getProperty("line.separator")}")
      println(s"You entered: $raw")
      val input = Try{LocalDate.parse(raw, DateTimeFormatter.ISO_LOCAL_DATE)}.toOption
      if (input.isDefined) {
        res = input
        println("I parsed: " + input.get.toString)
      } else {
        LocalDate.parse(raw, DateTimeFormatter.ISO_LOCAL_DATE)
        println("Incorrect input! Try again.")
      }
    }
    res.get
  }
}
