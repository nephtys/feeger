package Implementations

/**
  * Created by Christopher on 28.04.2016.
  */
trait SewobeConfigurable {
  def sewobeUsername : String
  def sewobePassword : String
  def sewobeQuery : Int
  def sewobeUrl : String

  def toFormString : String = s"USERNAME=$sewobeUsername&PASSWORT=$sewobePassword&AUSWERTUNG_ID=$sewobeQuery"
}
