package Implementations

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import Interfaces.{MinimalUser, RestCallable}
import spray.json._
import spray.json.DefaultJsonProtocol

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scalaj.http.Http
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Try

/**
  * Created by Christopher on 28.04.2016.
  */
class SewobeCaller extends RestCallable {

  def call()(implicit config : Configuration) : IndexedSeq[MinimalUser] = {
    Await.result(apply(), Duration.create(1, "minute"))
  }

  private val NICKNAME = "BIERNAME"
  private val FIRSTNAME = "VORNAME-PRIVATPERSON"
  private val SURNAME = "NACHNAME"
  private val MEMBERID = "NR"
  private val WANTSDEBIT = "ABBBUCHUNGSERLAUBNIS"
  private val DATEOFJOIN = "EINTRITT"
  private val CATEGORY = "UNTERKATEGORIE"

  private def translateWeirdSewobeJSONToUseableJSON(in: String): String = {
    val split: Seq[String] = in.split("\"DATENSATZ\":").flatMap(s => s.split("}}")).filter(s => s.length > 10)
    "[" + split.mkString("},") + "}]"
  }

  private def inapply()(implicit config : Configuration): Future[Seq[Map[String, String]]] = Future {
    val req = Http(config.sewobeUrl).header("Content-Type", "application/x-www-form-urlencoded").postData(config.toFormString).asString
    if (req.is2xx) {
      println("success")
      import DefaultJsonProtocol._
      translateWeirdSewobeJSONToUseableJSON(req.body).parseJson.convertTo[Seq[Map[String, String]]]
    } else {
      println("fail")
      Seq.empty[Map[String, String]]
    }
  }

  private val dateFormatter = DateTimeFormatter.ISO_LOCAL_DATE

  private def apply()(implicit config: Configuration) : Future[IndexedSeq[MinimalUser]] = {
    inapply().map(a => a.map(b => MinimalUser(nickname = b.get(NICKNAME).filter(_.trim.nonEmpty).map(_.trim), firstName = b.get(FIRSTNAME).filter(_.trim.nonEmpty).map(_.trim), surname = b.get(SURNAME).filter(_.trim.nonEmpty).map(_.trim), category = b.get(CATEGORY).map(_.trim).getOrElse(""), memberID = b(MEMBERID).toLong,usesDebitSystem = b(WANTSDEBIT).trim == "1", dateOfJoin = Try{LocalDate.parse(b(DATEOFJOIN), dateFormatter)}.toOption.getOrElse(LocalDate.MIN))).toIndexedSeq)
  }

}
