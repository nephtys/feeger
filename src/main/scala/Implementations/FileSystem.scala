package Implementations

import java.io.{File, PrintWriter}

import Interfaces.FileSystemAccess
import scala.io.Source

import scala.io.Source
import scala.util.Try

/**
  * Created by Christopher on 28.04.2016.
  */
class FileSystem extends FileSystemAccess {
  override def getSingleFile(filepath: String): Option[String] = Try{Source.fromFile(filepath).mkString}.toOption

  override def writeSingleFile(filepath: String, text: String): Boolean = {
    var b = false
    Some(new PrintWriter(filepath)).foreach{p => p.write(text); p.close(); b = true}
    b
  }
}
