package Implementations

import java.io.PrintWriter

import Interfaces.FileSystemAccess
import spray.json._
import spray.json.DefaultJsonProtocol

import scala.io.Source
import scala.util.Try

/**
  * Created by Christopher on 28.04.2016.
  */
object ConfigurationLoader {
  object ConfigurationJsonProtocol extends DefaultJsonProtocol {
    implicit val colorFormat = jsonFormat10(Configuration)
  }

  import ConfigurationJsonProtocol._

  def load(filepath : String)(implicit fsa : FileSystemAccess) : Option[Configuration] = {
    Try{fsa.getSingleFile(filepath).get.parseJson.convertTo[Configuration]}.toOption
  }


  def saveIfNotExistsAndLoad(filepath : String)(implicit fsa : FileSystemAccess) : Configuration = {
    val fromFile = Try{fsa.getSingleFile(filepath).get.parseJson.convertTo[Configuration]}.toOption
    if (fromFile.isDefined) {
      println("config already existing! loading from disk...")
      fromFile.get
    } else {
      println("no config existing! loading fresh and storing on disk...")
      println("... you should at least enter REST URL and password into the config file: " + filepath)
      val fromNothing = Configuration()
      fsa.writeSingleFile(filepath, fromNothing.toJson.prettyPrint)
      fromNothing
    }
  }

}
