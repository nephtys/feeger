package Interfaces

/**
  * Created by Christopher on 28.04.2016.
  */
trait Quizzable {
  def ask[T](question : String) : T
}
