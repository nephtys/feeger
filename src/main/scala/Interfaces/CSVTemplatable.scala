package Interfaces

import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
  * Created by Christopher on 28.04.2016.
  */
object CSVTemplatable {

  def fill(users : Seq[MinimalUser], title : String, description : String, priceInCents : Int, priceDecimalDelimiter : String, date : LocalDate) : Seq[Seq[String]] = {
    Seq(csvheader) ++ users.sortBy(_.memberID).map(u => (0 to 20).map(i => cell(i)(u, title, description, priceInCents, priceDecimalDelimiter, date)))
  }


  def formatCent(cents : Int)(priceDecimalDelimiter : String) : String = {
    val euros : Int = cents / 100
    val leftCents : Int = cents % 100
    euros.toString + priceDecimalDelimiter + leftCents.toString
  }

  private val csvheader = Seq("Mitgliedsnummer", "R vs G", "Rechnungsnr.",
    "Rechnungsname", "Rechnungsdatum", "Positionsnr.", "Positionsname", "Positionsbeschreibung",
    "Anzahl", "Preis pro Einheit", "2 == Lastschrift und 1 == Ueberweisung", "Empfang per Mail",
    "Zahlungsziel in Tagen", "SEPA Intervall (0 fuer einmalig)", "Datum Rechnungsstellung",
    "Datum Faelligkeit", "Datum Positionsende", "Mehrwertsteuersatz", "Buchhaltungskonto",
    "Steuerschluessel", "Unterkonto Aktivitas"
  )

  private val dtfrmt = DateTimeFormatter.ofPattern("dd.MM.yyyy")

  private def cell(index : Int)(user : MinimalUser, title : String, description : String, priceInCents : Int, priceDecimalDelimiter : String, date : LocalDate) : String = index match {
    case 0 => user.memberID.toString
    case 1 => "2"
    case 2 => "1"
    case 3 => "Vereinsbeitrag"
    case 4 => dtfrmt.format(date)
    case 5 => "1"
    case 6 => title//6 chargeTitle
    case 7 => description//7 chargeDescrpt
    case 8 => "1"
    case 9 => formatCent(priceInCents)(priceDecimalDelimiter)//9 price in euro
    case 10 => if (user.usesDebitSystem) "2" else "1" //10 manual charge (1) or debit (2)
    case 11 => "2"
    case 12 => "30"
    case 13 => "0"
    case 14 => dtfrmt.format(date.plusDays(2))//14 date when debit is going to be paid (+2 days)
    case 15 => dtfrmt.format(date.plusDays(7))//15 additional date (+7 days)
    case 16 => "31.12.2099"
    case 17 => "0"
    case 18 => "1112"
    case 19 => "1"
    case 20 => "8293"
    case _ => throw new IllegalArgumentException
  }

  assert(csvheader.length == 21)

}
