package Interfaces

/**
  * Created by Christopher on 28.04.2016.
  */
trait FileSystemAccess {
  def getSingleFile(filepath : String) : Option[String]
  def writeSingleFile(filepath : String, text : String) : Boolean
}
