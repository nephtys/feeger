package Interfaces

import Implementations.Configuration

/**
  * Created by Christopher on 28.04.2016.
  */
trait RestCallable {
  def call()(implicit config : Configuration) : Seq[MinimalUser]
}
