package Interfaces

import java.time.LocalDate

/**
  * Created by Christopher on 28.04.2016.
  */
case class MinimalUser(nickname : Option[String], firstName : Option[String], surname : Option[String], category : String, memberID : Long, usesDebitSystem : Boolean, dateOfJoin : LocalDate) {

}