import java.time.LocalDate
import java.time.format.DateTimeFormatter

import Implementations._
import Interfaces.{CSVTemplatable, MinimalUser}

/**
  * Created by Christopher on 28.04.2016.
  */
object Main extends App {


  def removeNewMembersWithLessThanThreeMonths(inp : IndexedSeq[MinimalUser], date : LocalDate) : (IndexedSeq[MinimalUser], IndexedSeq[MinimalUser]) = {
    val dateThreeMonthsAgo = date.minusMonths(3)
    val x = inp.filter(_.dateOfJoin.isBefore(dateThreeMonthsAgo))
    (x, inp.filterNot(y => x.contains(y)))
  }

  def filterOutSpecificGroups(inp : IndexedSeq[MinimalUser])(implicit config : Configuration) : (IndexedSeq[MinimalUser]) = inp.filter(x => config.onlyChargeCats.contains(x.category))

  println("Starting FeeGeR...")
  implicit val fsa = new FileSystem
  implicit val configuration = ConfigurationLoader.saveIfNotExistsAndLoad("./feeger.config.json")
  private val rest = new SewobeCaller
  private val quizzer = new Quizzer
  val thrdformat = DateTimeFormatter.ofPattern("yyyyMMdd")

  val date = quizzer.askLocalDate("Please enter the date you want to charge at")

  println("...calling Sewobe...")

  val specificUserGrouped = filterOutSpecificGroups(rest.call())
  val usersToDebit = removeNewMembersWithLessThanThreeMonths(specificUserGrouped, date)

  println("Following users are too fresh and will not be charged:")
  println("===========")
  usersToDebit._2.foreach(t => println(s"${t.nickname.getOrElse("N/A")} - ${t.firstName.getOrElse("N/A")} - ${t.surname.getOrElse("N/A")}"))
  println("===========")
  println("Following users will be charged:")
  println("===========")
  usersToDebit._1.foreach(t => println(s"${t.nickname.getOrElse("N/A")} - ${t.firstName.getOrElse("N/A")} - ${t.surname.getOrElse("N/A")}"))
  println("===========")
  println("...filling CSV and saving...")



  val csvTemp = CSVTemplatable.fill(usersToDebit._1, configuration.chargeTitle, configuration.chargeDesc, configuration.chargeCents, configuration.decimalDelimiter, date)

  println("Content of CSV:\n"+csvTemp.map(_.mkString(configuration.csvDelimiter)).mkString("\n"))


  println("... Write was successful: " + fsa.writeSingleFile(filepath = thrdformat.format(date) + ".csv", text = csvTemp.map(_.mkString(configuration.csvDelimiter)).mkString("\n")).toString + " ...")

  println("... shutting FeeGeR down.")
}
