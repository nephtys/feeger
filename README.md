# README #

This tool can be used to generate charges CSVs importable into Sewobe powered web portals.

### How to use? ###

* This is compiled and can be run with [sbt (Simple Build Tool)](http://www.scala-sbt.org/). You will need to have it installed on your machine.
* In the root project directory (containing folders e.g. 'src' and 'project') you open a terminal/shell/cmd.
* Execute 'sbt run' to prepare the project and execute the tool automatically
* On your first startup, a local configuration file 'feeger.config.json' will be created. It is missing the target URL and the required password. Stop the execution at this point and enter the right data into that file manually. It cannot work without those informations.
* On your following runs, you will be asked to enter a charge date in the format '2016-02-29'. Afterwards it will automatically create a finished CSV file in the same directory named '20160229.csv'


### How to compile into a fat JAR ###
* Execute 'sbt assembly' to build a self-contained 'fat' JAR than can be run without sbt or scala.
* You should run the JAR from within a terminal/cmd to be able to input the date. Doubleclicking the JAR does not work with javaw.exe.


### I have problems / encountered a bug. What to do? ###
* Please create a new Bug Tracker Issue for open questions or bugs